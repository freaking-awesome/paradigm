export const charParser = (str: string, length: number) => {
    return str.slice(0, length) + '...'
}