import { useEffect, useState } from 'react'
import Button from '../components/buttons/Button'
import Footer from '../components/footer/footer'
import Navbar from '../components/navbar/navbar'

export default function DefaultLayout({
  heroClass,
  heroChildren,
  children,
}: any) {
  const [scrollPosition, setScrollPosition] = useState(0)
  const [isTransparent, setIsTransparent] = useState(false)
  const handleScroll = () => {
    const position = window.scrollY
    setScrollPosition(position)
  }

  const scrollToTop = () => {
    setTimeout(() => {
      window.scrollTo({ top: 250, behavior: 'smooth' })
    }, 50)
    setTimeout(() => {
      window.scrollTo({ top: 0, behavior: 'smooth' })
    }, 100)
  }

  useEffect(() => {
    window.addEventListener('scroll', handleScroll, { passive: true })

    if (scrollPosition > 250) setIsTransparent(true)
    else setIsTransparent(false)

    return () => {
      window.removeEventListener('scroll', handleScroll)
    }
  })

  return (
    <>
      {scrollPosition > 250 ? (
        <div className='scroll-top fixed z-50 bottom-14 text-primary'>
          <Button onClick={scrollToTop} size='lg' color='darks' rounded={true}>
            Back To Top
          </Button>
        </div>
      ) : (
        <></>
      )}

      <div className={`${heroClass} w-screen`}>
        {/* navbar */}
        <Navbar isTransparent={isTransparent} />
        {/* end of navbar */}
        {/* hero */}
        {heroChildren}
        {/* end of hero */}
      </div>

      {/* render the page */}
      {children}

      {/* footer */}
      <Footer />
      {/* end of footer */}
    </>
  )
}
