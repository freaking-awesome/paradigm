import Button from '../buttons/Button'

export default function BackToTop(props: any) {
  const backToTop = () => {}

  return (
    <>
      <div className='fixed bottom-14 right-28'>
        <Button size='lg' color='darks' rounded={true}>
          Back To Top
        </Button>
      </div>
    </>
  )
}
