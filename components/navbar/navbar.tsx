import Link from 'next/link'
import Button from '../buttons/Button'
import ParadigmLogo from '../logo/paradigm-logo'
import NavbarItem from './navbar-item'

interface NavbarProps {
  isTransparent?: boolean
}

const Navbar = (props: NavbarProps) => {
  return (
    <>
      <div
        className={`px-28 py-5 navbar transition-all ease-in ${
          props.isTransparent
            ? 'bg-darks fixed top-0 z-50 flex justify-between items-center'
            : 'flex justify-between items-center'
        }`}
      >
        <div>
          <Link href='/'>
            <ParadigmLogo></ParadigmLogo>
          </Link>
        </div>

        <div>
          <div className='flex flex-row font-semibold right-0'>
            <NavbarItem title='About Us' link='/about' />
            <NavbarItem title='Events' link='/events' />
            <NavbarItem title='Partnerships' link='/partnership' />
            <Link href='/registration' replace>
              <Button color='primary' size='lg' rounded={true}>
                Register Now!
              </Button>
            </Link>
          </div>
        </div>
      </div>
    </>
  )
}

export default Navbar
