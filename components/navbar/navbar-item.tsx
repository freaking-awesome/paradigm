import Link from 'next/link'

interface NavbarItemProps {
  title?: string
  link: string
  children?: any
}

const NavbarItem = (props: NavbarItemProps) => {
  return (
    <>
      <div className='px-3 my-2 mx-2 cursor-pointer'>
        <Link href={props.link} className='hover:cursor-pointer'>
          <span className='navbar-item'>
            {props.title} {props.children}
          </span>
        </Link>
      </div>
    </>
  )
}

export default NavbarItem
