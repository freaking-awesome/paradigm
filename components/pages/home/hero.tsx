import { useRouter } from 'next/router'

const HeroChildren = () => {
  const router = useRouter()
  const scrollTo = () => {
    router.push('#event')
  }

  return (
    <>
      <div className='px-28 py-6'>
        {/* hero */}
        <div className='mt-20 w-full'>
          <div className='flex flex-col w-full justify-center items-center'>
            <div className='hero-text px-5 w-11/12'>
              <h1 className='text-hero text-center font-bold'>
                Let&apos;s Join Paradigm Unpad&apos;s Renowned Wave
              </h1>
              <p className='text-center text-xl mt-5'>
                Join Paradigm Unpad&apos;s pre-event by clicking button above.
              </p>
            </div>
          </div>

          <div className='w-full flex justify-center items-center mt-10'>
            <iframe
              className='rounded-lg w-full h-hero-video'
              src='https://www.youtube.com/embed/_dVjEQf68So?controls=0'
              title='YouTube video player'
              frameBorder='0'
              allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
              allowFullScreen
            ></iframe>
          </div>

          <div className='w-full flex justify-center items-center mt-10'>
            <button
              onClick={scrollTo}
              className='btn-secondary w-96 h-12 rounded-full px-7 py-2 font-bold'
            >
              See Closest Event
            </button>
          </div>
          {/* end of hero */}
        </div>
      </div>
    </>
  )
}

export default HeroChildren
