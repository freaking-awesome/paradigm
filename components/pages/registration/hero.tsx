import Image from 'next/image'
import { useState } from 'react'
import Vector from '../../../public/Vector.svg'
import InputGroup from '../../input/input-group'
import InputLabel from '../../input/input-label'

export default function RegistrationHero() {
  const eventTypes = ['Pre-Event 1', 'Mini Case Challenge', 'PBCC']

  const [form, setForm] = useState({
    event: 'Pre-Event 1',
    firstname: '',
    lastname: '',
    email: '',
    phone: '',
    univOrInst: '',
    major: '',
    address: '',
    refferal: '',
  })

  const classNameGenerator = (eventType: string) => {
    let defaultClass =
      'py-2.5 w-full font-semibold h-full cursor-pointer rounded transition-all'

    if (eventType === form.event) {
      defaultClass += ' border-b-4 border-b-primary text-primary'
    } else {
      defaultClass += ' text-light3'
    }

    if (eventType !== 'PBCC') {
      defaultClass += ' mr-5'
    }

    return defaultClass
  }

  return (
    <div className='w-full flex justify-center items-center'>
      <div className='mx-5 mb-10 mt-16 px-10 py-10 w-3/4 bg-sdark rounded-xl'>
        <div>
          <h3 className='text-4xl font-bold'>Registration Form</h3>
          <p className='py-5 text-secondary-text text-light'>
            Just find your desired event? Let&apos;s register by filling the
            form below!
          </p>
        </div>

        <div className='bg-info rounded-xl px-3 py-3 text-sinfo'>
          <div className='flex justify-start items-center'>
            <Image src={Vector} alt='icons' />

            <span className='ml-3'>Available Events:</span>
          </div>
          <div className='ml-12'>
            <ul className='list-disc'>
              <li>Pre-Event 1</li>
              <li>Mini Case Challange</li>
              <li>Padjajaran Business Case Competition</li>
            </ul>
          </div>
        </div>

        {/* divide line */}
        <div className='border-b-2 border-dark3 my-7'></div>
        {/* end of divide line */}

        <div className='flex justify-between items-center text-center'>
          {eventTypes.map((eventType, index) => (
            <div
              onClick={() => setForm({ ...form, event: eventType })}
              key={index}
              className={classNameGenerator(eventType)}
            >
              {eventType}
            </div>
          ))}
        </div>

        <div className='pt-8'>
          <div className='grid grid-cols-2 gap-6'>
            <InputGroup
              id='firstname'
              htmlFor='#firstname'
              label='First Name'
              name='firstname'
              placeholder='e.g. Fulan'
              type='text'
              isRequired={true}
            />

            <InputGroup
              id='lastname'
              htmlFor='#lastname'
              label='Last Name'
              name='lastname'
              placeholder='e.g. Fulan'
              type='text'
              isRequired={false}
            />

            <InputGroup
              id='email'
              htmlFor='#email'
              label='Email Address'
              name='email'
              placeholder='e.g. fulan@domain.com'
              type='email'
              isRequired={true}
            />

            <div className='flex flex-col'>
              <InputLabel htmlFor='#phone' isRequired={true}>
                Phone
              </InputLabel>

              <div className='flex justify-start items-center'>
                <select className='text-light3 px-4 py-2 rounded-lg bg-dark2 focus:outline-none'>
                  <option value='+62'>+62</option>
                </select>
                <input
                  type='text'
                  name='phone'
                  id='phone'
                  required
                  placeholder='8138527223333'
                  className='ml-2 w-full px-4 py-2 rounded-lg bg-dark2'
                />
              </div>
            </div>
          </div>

          <div className='grid grid-cols-1 gap-y-6 mt-6'>
            <InputGroup
              label='University or Institution'
              isRequired={true}
              htmlFor='#univOrInst'
              id='univOrInst'
              name='univOrInst'
              placeholder='e.g. University X, Polytechnic of Y'
              type='text'
            />

            <InputGroup
              label='Major'
              isRequired={true}
              htmlFor='#major'
              id='major'
              name='major'
              placeholder='e.g. Management, Digital Business'
              type='text'
            />

            <InputGroup
              label='Address'
              isRequired={true}
              htmlFor='#address'
              id='address'
              name='address'
              placeholder='e.g. Jl. MH. Thamrin No. 29'
              type='text'
            />
          </div>
        </div>
      </div>
    </div>
  )
}
