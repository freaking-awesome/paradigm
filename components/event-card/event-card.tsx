import Image, { StaticImageData } from 'next/image'
import { charParser } from '../../utils/db/char-parser'
import Button from '../buttons/Button'
import Card from '../card/card'
import CardTitle from '../card/card-title'

interface Event {
  title: string
  img: string | StaticImageData
  dueDate: string
  description: string
}

interface EventCardProps {
  event: Event
}

const EventCard = (props: EventCardProps) => {
  return (
    <>
      <Card color='sdark'>
        <Image src={props.event.img} alt='event' width={483} />
        <CardTitle>{props.event.title}</CardTitle>
        <h4 className='text-gray-500 text-center'>27 Agustus 2022</h4>
        <p className='text-gray-500 text-center mt-3'>
          {charParser(props.event.description, 101)}
        </p>

        <div className='flex justify-center items-center mt-5'>
          <Button color='primary' size='lg' rounded={true}>
            <p className='font-bold'>Read More</p>
          </Button>
        </div>
      </Card>
    </>
  )
}

export default EventCard
