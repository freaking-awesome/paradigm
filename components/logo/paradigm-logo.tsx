import Image from 'next/image'
import Link from 'next/link'
import Logo from '../../public/logo.svg'

const ParadigmLogo = () => {
  return (
    <Link href='/' replace>
      <Image src={Logo} alt='paradigm-logo' className='cursor-pointer'></Image>
    </Link>
  )
}

export default ParadigmLogo
