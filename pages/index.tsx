import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import Navbar from '../components/navbar/navbar'
import * as moment from 'moment-timezone'
import Card from '../components/card/card'
import event from '../public/event.png'
import ambassador from '../public/ambassador.jpg'
import EventCard from '../components/event-card/event-card'
import Footer from '../components/footer/footer'
import { useEffect, useState } from 'react'
import Button from '../components/buttons/Button'
import DefaultLayout from '../layouts'
import HeroChildren from '../components/pages/home/hero'
// import HeroBackground from '../public/hero-bg.jpg'

const Home: NextPage = () => {
  const eventsSlider = [
    {
      title: 'Pre-Event 1: The Secrets Behind Highly Productive People',
      dueDate: moment.tz('Asia/Jakarta').format('DD MMMM YYYY'),
      description:
        'Teknik dasar lempar lembing setidaknya terdiri dari tiga bagian, yakni teknik memegang lembing, teknik berlari dan membawa lembing Teknik dasar lempar lembing setidaknya terdiri dari tiga bagian, yakni teknik memegang lembing, teknik berlari dan membawa lembing',
      img: event,
    },
    {
      title: 'Pre-Event 1: The Secrets Behind Highly Productive People',
      dueDate: moment.tz('Asia/Jakarta').format('DD MMMM YYYY'),
      description:
        'Teknik dasar lempar lembing setidaknya terdiri dari tiga bagian, yakni teknik memegang lembing, teknik berlari dan membawa lembing Teknik dasar lempar lembing setidaknya terdiri dari tiga bagian, yakni teknik memegang lembing, teknik berlari dan membawa lembing',
      img: event,
    },
    {
      title: 'Pre-Event 1: The Secrets Behind Highly Productive People',
      dueDate: moment.tz('Asia/Jakarta').format('DD MMMM YYYY'),
      description:
        'Teknik dasar lempar lembing setidaknya terdiri dari tiga bagian, yakni teknik memegang lembing, teknik berlari dan membawa lembing Teknik dasar lempar lembing setidaknya terdiri dari tiga bagian, yakni teknik memegang lembing, teknik berlari dan membawa lembing',
      img: event,
    },
    {
      title: 'Pre-Event 1: The Secrets Behind Highly Productive People',
      dueDate: moment.tz('Asia/Jakarta').format('DD MMMM YYYY'),
      description:
        'Teknik dasar lempar lembing setidaknya terdiri dari tiga bagian, yakni teknik memegang lembing, teknik berlari dan membawa lembing Teknik dasar lempar lembing setidaknya terdiri dari tiga bagian, yakni teknik memegang lembing, teknik berlari dan membawa lembing',
      img: event,
    },
  ]

  return (
    <>
      <Head>
        <title>Paradigm Unpad</title>
      </Head>
      <DefaultLayout heroClass='navbar-hero' heroChildren={<HeroChildren />}>
        <div className='px-28' id='event'>
          {/* divide line */}
          <div className='border-b-2 border-sdark mt-16'></div>
          {/* end of divide line */}

          {/* another section */}
          <div className='text-white w-full h-max overflow-hidden'>
            <div className='flex flex-col w-full justify-center items-center my-8'>
              <h3 className='text-4xl font-bold'>Closest Event</h3>
              <h5 className='text-2xl'>
                Closest Event Don&apos;t want to miss out? Below are our closest
                and latest event for you to join.
              </h5>
            </div>

            <div className='flex flex-row items-center w-max'>
              {eventsSlider.map((e, i) => {
                return <EventCard key={i} event={e} />
              })}
            </div>
          </div>
          {/* end of another section */}
        </div>
        {/* another section */}
        <div className='bg-sdark overflow-scroll mt-10'>
          <div className='flex flex-col my-5 py-5 px-28'>
            <h3 className='font-bold text-center text-4xl text-white  mt-10'>
              Student Ambassador
            </h3>

            <div className='flex flex-row justify-center items-center w-max h-full my-8'>
              <Image
                className='rounded-lg shadow-lg'
                src={ambassador}
                alt='student ambassador'
              />
              <Image
                className='rounded-lg shadow-lg'
                src={ambassador}
                alt='student ambassador'
              />
              <Image
                className='rounded-lg shadow-lg'
                src={ambassador}
                alt='student ambassador'
              />
              <Image
                className='rounded-lg shadow-lg'
                src={ambassador}
                alt='student ambassador'
              />
              <Image
                className='rounded-lg shadow-lg'
                src={ambassador}
                alt='student ambassador'
              />
              <Image
                className='rounded-lg shadow-lg'
                src={ambassador}
                alt='student ambassador'
              />
              <Image
                className='rounded-lg shadow-lg'
                src={ambassador}
                alt='student ambassador'
              />
            </div>
          </div>

          <div className='flex flex-col my-5 py-5 px-28'>
            <h3 className='font-bold text-center text-4xl text-white  mt-10'>
              Last Year Winner
            </h3>

            <div className='flex flex-row-reverse place-items-end justify-end items-end w-max h-full my-8'>
              <Image
                className='rounded-lg shadow-lg'
                src={ambassador}
                alt='student ambassador'
              />
              <Image
                className='rounded-lg shadow-lg'
                src={ambassador}
                alt='student ambassador'
              />
              <Image
                className='rounded-lg shadow-lg'
                src={ambassador}
                alt='student ambassador'
              />
              <Image
                className='rounded-lg shadow-lg'
                src={ambassador}
                alt='student ambassador'
              />
              <Image
                className='rounded-lg shadow-lg'
                src={ambassador}
                alt='student ambassador'
              />
              <Image
                className='rounded-lg shadow-lg'
                src={ambassador}
                alt='student ambassador'
              />
              <Image
                className='rounded-lg shadow-lg'
                src={ambassador}
                alt='student ambassador'
              />
            </div>
          </div>
        </div>
        {/* end of another section */}

        {/* another section */}
        <div className='bg-darks'>
          <div className='flex flex-col justify-center items-center my-10'>
            <div className='bg-sdark rounded-lg my-8'>
              <div className='px-8 py-5 w-max'>
                <h3 className='font-bold text-center text-white text-4xl'>
                  Sponsors
                </h3>

                <div className='grid my-5 grid-cols-3 gap-4'>
                  <Image
                    src={ambassador}
                    width={250}
                    height={250}
                    alt='sponsor'
                  ></Image>
                  <Image
                    src={ambassador}
                    width={250}
                    height={250}
                    alt='sponsor'
                  ></Image>
                  <Image
                    src={ambassador}
                    width={250}
                    height={250}
                    alt='sponsor'
                  ></Image>
                  <Image
                    src={ambassador}
                    width={250}
                    height={250}
                    alt='sponsor'
                  ></Image>
                  <Image
                    src={ambassador}
                    width={250}
                    height={250}
                    alt='sponsor'
                  ></Image>
                  <Image
                    src={ambassador}
                    width={250}
                    height={250}
                    alt='sponsor'
                  ></Image>
                </div>
              </div>
            </div>

            <div className='bg-sdark rounded-lg my-5'>
              <div className='px-8 py-5 w-max'>
                <h3 className='font-bold text-center text-white text-4xl'>
                  Partners
                </h3>

                <div className='grid my-5 grid-cols-3 gap-4'>
                  <Image
                    src={ambassador}
                    width={250}
                    height={250}
                    alt='sponsor'
                  ></Image>
                  <Image
                    src={ambassador}
                    width={250}
                    height={250}
                    alt='sponsor'
                  ></Image>
                  <Image
                    src={ambassador}
                    width={250}
                    height={250}
                    alt='sponsor'
                  ></Image>
                  <Image
                    src={ambassador}
                    width={250}
                    height={250}
                    alt='sponsor'
                  ></Image>
                  <Image
                    src={ambassador}
                    width={250}
                    height={250}
                    alt='sponsor'
                  ></Image>
                  <Image
                    src={ambassador}
                    width={250}
                    height={250}
                    alt='sponsor'
                  ></Image>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* end of another section */}
      </DefaultLayout>
    </>
  )
}

export default Home
