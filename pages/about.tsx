import { NextPage } from 'next'
import Head from 'next/head'
import DefaultLayout from '../layouts'

const About: NextPage = () => {
  return (
    <>
      <Head>
        <title>Paradigm Unpad</title>
      </Head>

      <DefaultLayout></DefaultLayout>
    </>
  )
}

export default About
