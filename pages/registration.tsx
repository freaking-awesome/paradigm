import Head from 'next/head'
import { useRouter } from 'next/router'
import Navbar from '../components/navbar/navbar'
import RegistrationHero from '../components/pages/registration/hero'
import DefaultLayout from '../layouts'

export default function Registration() {
  return (
    <>
      <Head>
        <title>Event Registration</title>
      </Head>

      <DefaultLayout
        heroChildren={<RegistrationHero />}
        heroClass='navbar-hero'
      ></DefaultLayout>
    </>
  )
}
